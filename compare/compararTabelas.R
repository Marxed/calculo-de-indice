#comparar tabelas

coPath <-  getwd()

t1 <- paste(coPath, "/extraCompleta.csv", sep = "") 
t2 <- paste(coPath, "/inventa.csv", sep = "") 


table1 <-  read.csv2(t1, header = TRUE, sep = ';', dec = ',')
table2 <-  read.csv2(t2, header = TRUE, sep = ';', dec = ',')

qtd_linhas <- nrow(table1)
qtd_linhas1 <- nrow(table2)

joinTable <- matrix(0, qtd_linhas1, 3)
joinTable <-data.frame(joinTable)

col_headings <- c('nome_cientifico','nome_popular','Dap')
names(joinTable) <- col_headings

joinTable[,1:2] <- table2[,1:2]

joinTable$nome_cientifico <- as.character(joinTable$nome_cientifico)
joinTable$nome_popular <- as.character(joinTable$nome_popular)

table1$Nome.Popular <- as.character(table1$Nome.Popular)
table1$Nome.Cient�fico <- as.character(table1$Nome.Cient�fico)
table1$DAP.cm. <- as.double(table1$DAP.cm.)




for (a in 1:qtd_linhas1) {
  
  for (b in 1:qtd_linhas) {
    
    if(table1[b,4] == joinTable[a,1]){
      
      joinTable[a,3] <- table1[b,5]
      
    } 
  }
}


#Tirar os repetidos e salvar csv

# teste1 <- table1[!duplicated(table1[,1]),]
# index <- which(duplicated(table1[,1]))
# boraver <-  table1[-index, ]
# write.csv(boraver,file = "boraver.csv", row.names = FALSE)

#Teste da fun��o 'duplicated', comparando resultados retornados

# teste <- table1[duplicated(table1[,1]),]
# teste1 <- table1[!duplicated(table1[,1]),]
