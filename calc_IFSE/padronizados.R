for(colu in 1:ncol(testre)){
  
  Fmax = max(testre[,colu])
  Fmin = min(testre[,colu]) 
  
  for(lin in 1:nrow(testre)){
    
    Fi <- testre[lin,colu]
    
    Fp <- ((Fi - Fmin)/(Fmax - Fmin))
    
    testre[lin,colu] = Fp	 
    
  }
}

