library(psych)

inventario <- read.csv2('C:/Users/Marcus/Desktop/calc_IFSE/teste2.csv', header = TRUE, sep = ';', dec = ',')

inventario$abr <- as.character(inventario$abr)
inventario$abr <- as.numeric(inventario$abr)

inventario$fqr <- as.character(inventario$fqr)
inventario$fqr <- as.numeric(inventario$fqr)

inventario$dor <- as.character(inventario$dor)
inventario$dor <- as.numeric(inventario$dor)

inventario$vcmr <- as.character(inventario$vcmr)
inventario$vcmr <- as.numeric(inventario$vcmr)

inventario$biomr <- as.character(inventario$biomr)
inventario$biomr <- as.numeric(inventario$biomr)

inventario$usor <- as.character(inventario$usor)
inventario$usor <- as.numeric(inventario$usor)

inf1 <- sapply(inventario, summary)

inf2 <- sapply(inventario, sd)

inf3 <-  cor(inventario)

fit <- princomp(inventario, cor = TRUE)

sumario <- summary(fit)

carregados <- loadings(fit)

# plot(fit, type="line")

# fit$scores


tabela <- as.data.frame(fit$scores)

# biplot(fit)

sumarioTabela <- summary(tabela)

fit <- principal(inventario, nfactors = 3, rotate = "varimax", scores = TRUE)

fatores <- as.data.frame(fit$scores)

fatoresLinhas <- nrow(fatores)

fatoresPz <- matrix(0, fatoresLinhas,3)

fatoresPz[,1] <- fatores[,1]
fatoresPz[,2] <- fatores[,2]
fatoresPz[,3] <- fatores[,3]

for (p in 1:3) {
  
  for (l in 1:fatoresLinhas) {
    
    fatoresPz[l,p] <- ((fatores[l,p] - min(fatores[,p]))/(max(fatores[,p]) - min(fatores[,p])))
    
  }
}

loadings_fac1 = fit$loadings[,1]
l1 = sum(loadings_fac1^2)

loadings_fac1 = fit$loadings[,2]
l2 = sum(loadings_fac1^2)

loadings_fac1 = fit$loadings[,3]
l3 = sum(loadings_fac1^2)

somaLT <- l1 + l2 + l3

lb1 = l1/somaLT
lb2 = l2/somaLT
lb3 = l3/somaLT

fatoresPVLB <- matrix(0, fatoresLinhas, 3)

fatoresPVLB[, 1] <- fatoresPz[, 1] * lb1
fatoresPVLB[, 2] <- fatoresPz[, 2] * lb2
fatoresPVLB[, 3] <- fatoresPz[, 3] * lb3

ifse <- matrix(0, fatoresLinhas, 1)

for (k in 1:fatoresLinhas) {
  
  ifse[k,] <- fatoresPVLB[k,1] + fatoresPVLB[k,2] + fatoresPVLB[k,3]
}

ifsePor <- matrix(0, fatoresLinhas, 1)

for (z in 1:fatoresLinhas) {
  ifsePor[z,] <- (ifse[z,]/sum(ifse))*100
}

ampcat <- ((max(ifse)- min(ifse))/3)
catlms1 <- max(ifse)
catlmi1 <- max(ifse) - ampcat

catlms3 <- min(ifse) + ampcat
catlmi3 <- min(ifse)

catlms2 <- catlmi1 - 0.001
catlmi2 <- catlms3 + 0.001

categorias <- matrix(0, fatoresLinhas, 1)

for (w in 1:fatoresLinhas) {
  
  if(ifse[w,1] <= catlms1 && ifse[w,1] >= catlmi1  ){
    categorias[w,1] <- 1
  } else if(ifse[w,1] <= catlms3 && ifse[w,1] >= catlmi3  ){
    categorias[w,1] <- 3
  } else{
    categorias[w,1] <- 2
  }
}
# library(xlsx) #load the package
# write.xlsx(x = tabela_completaD, file = "test.excelfile.xlsx",sheetName = "TestSheet", row.names = TRUE)

# loadings_fac1 = fit$loadings[,1]
# eigenv_fac1 = sum(loadings_fac1^2)